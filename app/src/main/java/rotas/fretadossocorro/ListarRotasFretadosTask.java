package rotas.fretadossocorro;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class ListarRotasFretadosTask extends AsyncTask<Void, Void, String> {
    private Context context;
    private ProgressDialog dialog;

    public ListarRotasFretadosTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        dialog = ProgressDialog.show(context, "Aguarde", "Buscando...", true, true);
    }

    @Override
    protected String doInBackground(Void... params) {
        FretadosService service = new FretadosService();
        return service.getAllRotas();
    }

    @Override
    protected void onPostExecute(String resposta) {
        dialog.dismiss();
    }
}
