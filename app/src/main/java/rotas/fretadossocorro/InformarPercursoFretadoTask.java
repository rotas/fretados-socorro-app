package rotas.fretadossocorro;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class InformarPercursoFretadoTask extends AsyncTask<Void, Void, String> {
    private Context context;
    private String percurso;
    private Integer idRota;
    private ProgressDialog dialog;

    public InformarPercursoFretadoTask(Context context, String percursoJson) {
        this.context = context;
        this.percurso = percursoJson;
        this.idRota = idRota;
    }

    @Override
    protected void onPreExecute() {
        dialog = ProgressDialog.show(context, "Aguarde", "Enviando...", true, true);
    }

    @Override
    protected String doInBackground(Void... params) {
        FretadosService service = new FretadosService();
        return service.informarPercurso(percurso);
    }

    @Override
    protected void onPostExecute(String resposta) {
        dialog.dismiss();
    }
}
