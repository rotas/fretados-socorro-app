package rotas.fretadossocorro;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final int LISTA_LINHAS_ACTIVITY = 0;
    private static final String REQUESTING_LOCATION_UPDATES_KEY = "rotasFretadoRequestingLocationUpdate";
    private static final String REQUESTING_PERCURSO_KEY = "rotasFretadoRequestingPercurso";
    private static final String REQUESTING_ROTA_KEY = "rotasFretadoRequestingRota";

    private boolean requestingLocationUpdates;

    private GoogleMap mMap;

    private static final String TAG = "Fretados Socorro";

    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private LocationRequest mLocationRequest;

    private Rota rotaSelecionada;
    private Percurso percurso = new Percurso();
    private TextView map_rotaSelecionada;
    private FloatingActionButton fabParada;
    private FloatingActionButton fabStart;
    private FloatingActionButton fabFinish;

    private BottomNavigationView navigation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        map_rotaSelecionada = findViewById(R.id.map_rotaSelecionada);

        requestingLocationUpdates = false;

        // Solicita as permissões
        String[] permissoes = new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.INTERNET
        };
        ObtemPermissoes.validate(this, 0, permissoes);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        createLocationRequest();

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {

                    adicionarPosicaoPercurso(TipoItemPercurso.WAYPOINT, location, false);
                    LatLng posicao = new LatLng(location.getLatitude(), location.getLongitude());
                    mMap.addMarker(new MarkerOptions().position(posicao).title("Você está aqui"));
                    //mMap.moveCamera(CameraUpdateFactory.newLatLng(posicao));
                    float zoomLevel = 16.0f;
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(posicao, zoomLevel));

                }
            }
        };

        navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        fabParada = findViewById(R.id.map_addParada);
        fabParada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                adicionarPosicaoPercurso(TipoItemPercurso.WAYPOINT, getLastKnownLocation(), true);
                toast("Parada adicionada" + getLastKnownLocation().toString());

            }
        });

        fabStart = findViewById(R.id.map_start);
        fabStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                adicionarPosicaoPercurso(TipoItemPercurso.PARTIDA, getLastKnownLocation(), true);
                startLocationUpdates();
                habilitaDesabilitaBotoes("start");

            }
        });

        fabFinish = findViewById(R.id.map_finish);
        fabFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                stopLocationUpdates();
                adicionarPosicaoPercurso(TipoItemPercurso.DESTINO, getLastKnownLocation(), true);

                Gson gson = new Gson();
                String percursoJson = gson.toJson(percurso);

                enviarPercurso(percursoJson);
                habilitaDesabilitaBotoes("finish");

            }
        });


        updateValuesFromBundle(savedInstanceState);

        habilitaDesabilitaBotoes("reset");

    }

    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            return;
        }

        // Update the value of requestingLocationUpdates from the Bundle.
        if (savedInstanceState.keySet().contains(REQUESTING_LOCATION_UPDATES_KEY)) {
            requestingLocationUpdates = savedInstanceState.getBoolean(
                    REQUESTING_LOCATION_UPDATES_KEY);
        }

        // Update the value of requestingLocationUpdates from the Bundle.
        if (savedInstanceState.keySet().contains(REQUESTING_PERCURSO_KEY)) {
            percurso = (Percurso) savedInstanceState.getSerializable(
                    REQUESTING_PERCURSO_KEY);
        }

        // Update the value of requestingLocationUpdates from the Bundle.
        if (savedInstanceState.keySet().contains(REQUESTING_ROTA_KEY)) {
            rotaSelecionada = (Rota) savedInstanceState.getSerializable(
                    REQUESTING_ROTA_KEY);
        }

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(REQUESTING_LOCATION_UPDATES_KEY,
                requestingLocationUpdates);
        outState.putSerializable(REQUESTING_PERCURSO_KEY,
                percurso);
        outState.putSerializable(REQUESTING_ROTA_KEY,
                rotaSelecionada);

        super.onSaveInstanceState(outState);
    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {


            habilitaDesabilitaBotoes( "tudo");

            switch (item.getItemId()) {

                case R.id.navigation_reset:

                    rotaSelecionada = null;
                    percurso = new Percurso();
                    requestingLocationUpdates = false;
                    map_rotaSelecionada.setText("");

                    habilitaDesabilitaBotoes("reset");
                    return true;

                case R.id.navigation_rota:

                    Intent intent = new Intent(getApplicationContext(), ListarRotasActivity.class);
                    startActivityForResult(intent, LISTA_LINHAS_ACTIVITY);
                    return true;

            }
            return false;
        }
    };


    @SuppressLint({"ResourceType", "SetTextI18n"})
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (LISTA_LINHAS_ACTIVITY): {
                if (resultCode == Activity.RESULT_OK) {
                    // TODO Extract the data returned from the child Activity.
                    rotaSelecionada = (Rota) data.getSerializableExtra("Rota");
                    map_rotaSelecionada.setText(rotaSelecionada.getId() + "-" + rotaSelecionada.getNome());

                    percurso.setRota_id(rotaSelecionada.getId());

                    habilitaDesabilitaBotoes("rota");

                }
                break;
            }
        }
    }

    protected void createLocationRequest() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(180000);
        mLocationRequest.setFastestInterval(150000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                // All location settings are satisfied. The client can initialize
                // location requests here.
                // ...
            }
        });

        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    // Solicita as permissões
                    String[] permissoes = new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    };
                    ObtemPermissoes.validate(MapsActivity.this, 0, permissoes);
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        //stopLocationUpdates();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (requestingLocationUpdates) {
            startLocationUpdates();
        }

    }


    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            toast("sem permissao startLocationUpdates");
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback,
                null /* Looper */);

    }

    private void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            LatLng posicao = new LatLng(location.getLatitude(), location.getLongitude());
                            mMap.addMarker(new MarkerOptions().position(posicao).title("Você está aqui"));
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(posicao));
                        }
                    }
                });

    }


    @SuppressLint("RestrictedApi")
    private void habilitaDesabilitaBotoes(String estado){
        if (estado.equals("reset")) {
            navigation.findViewById(R.id.navigation_reset).setVisibility(View.VISIBLE); //.setEnabled(true);
            navigation.findViewById(R.id.navigation_rota).setVisibility(View.VISIBLE); //.setEnabled(true);
            fabStart.setVisibility(View.INVISIBLE);//.setEnabled(false);
            fabFinish.setVisibility(View.INVISIBLE); //.setEnabled(false);
            fabParada.setVisibility(View.INVISIBLE);
        }
        if (estado.equals("rota")) {
            navigation.findViewById(R.id.navigation_reset).setVisibility(View.VISIBLE);
            navigation.findViewById(R.id.navigation_rota).setVisibility(View.GONE);
            fabStart.setVisibility(View.VISIBLE);
            fabFinish.setVisibility(View.INVISIBLE);
            fabParada.setVisibility(View.INVISIBLE);
        }
        if (estado.equals("start")) {
            navigation.findViewById(R.id.navigation_reset).setVisibility(View.VISIBLE);
            navigation.findViewById(R.id.navigation_rota).setVisibility(View.GONE);
            fabStart.setVisibility(View.INVISIBLE);
            fabFinish.setVisibility(View.VISIBLE);
            fabParada.setVisibility(View.VISIBLE);
        }
        if (estado.equals("finish")) {
            navigation.findViewById(R.id.navigation_reset).setVisibility(View.VISIBLE);
            navigation.findViewById(R.id.navigation_rota).setVisibility(View.GONE);
            fabStart.setVisibility(View.INVISIBLE);
            fabFinish.setVisibility(View.INVISIBLE);
            fabParada.setVisibility(View.INVISIBLE);
        }
        if (estado.equals("tudo")) {
            navigation.findViewById(R.id.navigation_reset).setVisibility(View.GONE);
            navigation.findViewById(R.id.navigation_rota).setVisibility(View.GONE);
            fabStart.setVisibility(View.INVISIBLE);
            fabFinish.setVisibility(View.INVISIBLE);
            fabParada.setVisibility(View.INVISIBLE);
        }

    }


    private void enviarPercurso(String percursoJson) {

        try {
            new InformarPercursoFretadoTask(this, percursoJson).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void adicionarPosicaoPercurso(TipoItemPercurso tipoItemPercurso, Location location, Boolean isParada) {

        if (tipoItemPercurso.equals(TipoItemPercurso.WAYPOINT)) {
            ArrayList<Object> waypoint = new ArrayList<>();

            waypoint.add(location.getLatitude());
            waypoint.add(location.getLongitude());
            if (isParada) {
                waypoint.add("parada");
            }
            percurso.waypoints.add(waypoint);

        }

        if (tipoItemPercurso.equals(TipoItemPercurso.PARTIDA)) {
            percurso.partida.add(location.getLatitude());
            percurso.partida.add(location.getLongitude());
        }

        if (tipoItemPercurso.equals(TipoItemPercurso.DESTINO)) {
            percurso.destino.add(location.getLatitude());
            percurso.destino.add(location.getLongitude());
        }

    }


    private Location getLastKnownLocation() {
        LocationManager mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            @SuppressLint("MissingPermission") Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    private void toast(String s) {
        Toast.makeText(getBaseContext(), s, Toast.LENGTH_SHORT).show();
    }

}
