package rotas.fretadossocorro;

public enum TipoItemPercurso {
    PARTIDA,
    WAYPOINT,
    DESTINO;
}
