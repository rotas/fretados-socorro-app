package rotas.fretadossocorro;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ListarRotasActivity extends AppCompatActivity {

    private ListView listaRotas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_rotas);

        listaRotas = (ListView) findViewById(R.id.lista_rotas);


        listaRotas.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapter, View view,
                                    int position, long id) {

                Rota rotaSelecionada = (Rota) adapter.getItemAtPosition(position);

                Intent resultIntent = new Intent();
                // TODO Add extras or a data URI to this intent as appropriate.
                resultIntent.putExtra("Rota", rotaSelecionada);
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();

        try {
            String rotasJson = new ListarRotasFretadosTask(this).execute().get();

            Type listType = new TypeToken<ArrayList<Rota>>(){}.getType();
            List<Rota> rotas =  new Gson().fromJson(rotasJson, listType);

            ArrayAdapter<Rota> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, rotas);
            listaRotas.setAdapter(adapter);

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
