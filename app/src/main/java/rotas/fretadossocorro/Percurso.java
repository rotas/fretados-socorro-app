package rotas.fretadossocorro;

import java.io.Serializable;
import java.util.ArrayList;

class Percurso implements Serializable {

    Integer id ;
    Integer rota_id;
    ArrayList<Object> partida = new ArrayList<Object>();
    ArrayList<Object> waypoints = new ArrayList<Object>();
    ArrayList<Object> destino = new ArrayList<Object>();


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    String created_at;
    String updated_at;
    String url;

    public Integer getRota_id() {
        return rota_id;
    }

    public void setRota_id(Integer rota_id) {
        this.rota_id = rota_id;
    }

    public ArrayList<Object> getPartida() {
        return partida;
    }

    public void setPartida(ArrayList<Object> partida) {
        this.partida = partida;
    }

    public ArrayList<Object> getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(ArrayList<Object> waypoints) {
        this.waypoints = waypoints;
    }

    public ArrayList<Object> getDestino() {
        return destino;
    }

    public void setDestino(ArrayList<Object> destino) {
        this.destino = destino;
    }

    @Override
    public String toString() {
        return "Percurso{" +
                "id=" + id +
                ", rota_id=" + rota_id +
                ", partida=" + partida +
                ", waypoints=" + waypoints +
                ", destino=" + destino +
                ", created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}