package rotas.fretadossocorro;

import java.io.Serializable;
import java.util.Objects;

public class Rota implements Serializable {
    private Integer id;
    private String nome;
    private String partida;
    private String tipo_veiculo;


    public Integer getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getPartida() {
        return partida;
    }

    public String getTipo_veiculo() {
        return tipo_veiculo;
    }


    public void setId(Integer id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setPartida(String partida) {
        this.partida = partida;
    }

    public void setTipo_veiculo(String tipo_veiculo) {
        this.tipo_veiculo = tipo_veiculo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rota rota = (Rota) o;
        return Objects.equals(id, rota.id) &&
                Objects.equals(nome, rota.nome) &&
                Objects.equals(partida, rota.partida) &&
                Objects.equals(tipo_veiculo, rota.tipo_veiculo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nome, partida, tipo_veiculo);
    }

    @Override
    public String toString() {
        return
                "Rota: " + id +
                "-" + nome + '\n' +
                "partida: " + partida + " " +
                "(" + tipo_veiculo  +
                ')';
    }
}
