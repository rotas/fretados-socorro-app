package rotas.fretadossocorro;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class FretadosService {
    String path_servico = "https://fretados-socorro.herokuapp.com";
    String path_recurso_lista_rotas = "/rotas.json";
    String path_percurso = "/percursos";


    public String getAllRotas() {
        try {
            URL url = new URL(path_servico + path_recurso_lista_rotas);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Accept", "application/json");

            connection.connect();

            //Create a new InputStreamReader
            InputStreamReader streamReader = new
            InputStreamReader(connection.getInputStream());

            //Create a new buffered reader and String Builder
            BufferedReader reader = new BufferedReader(streamReader);
            StringBuilder stringBuilder = new StringBuilder();

            //Check if the line we are reading is not null
            String inputLine;
            while((inputLine = reader.readLine()) != null){
                stringBuilder.append(inputLine);
            }

            //Close our InputStream and Buffered reader
            reader.close();
            streamReader.close();

            //Set our result equal to our stringBuilder
            String resposta = stringBuilder.toString();

            return resposta;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public Percurso iniciarPercurso() {
        try {
            URL url = new URL(path_servico + path_percurso);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Content-type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Authorization", "Token token=fretadoszonasul");
            connection.setRequestMethod("POST");

            connection.connect();

            Log.e("FretadosSocorro", "conexao:" + connection.getResponseCode());
            Scanner scanner = new Scanner(connection.getInputStream());
            String resposta = scanner.next();
            Log.e("FretadosSocorro", "inicio percurso: " + resposta);


            Type listType = new TypeToken<Percurso>(){}.getType();
            Percurso percurso=  new Gson().fromJson(resposta, listType);

            return percurso;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }




    public String informarPercurso(String json) {
        try {

            Integer idPercurso = iniciarPercurso().getId();
            String resposta = "";

            if (path_percurso != null) {

                URL url = new URL(path_servico + path_percurso + "/" + idPercurso );
                Log.e("FretadosSocorro", "url:" + url);

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                connection.setRequestProperty("Content-type", "application/json");
                connection.setRequestProperty("Accept", "application/json");
                connection.setRequestProperty("X-HTTP-Method-Override", "PATCH");
                connection.setRequestProperty("Authorization", "Token token=fretadoszonasul");
                connection.setRequestMethod("POST");

                connection.setDoOutput(true);

                PrintStream output = new PrintStream(connection.getOutputStream());
                Log.e("FretadosSocorro", "json a enviar: " + json);
                output.println(json);

                connection.connect();
                Log.e("FretadosSocorro", "conexao:" + connection.getResponseCode());


                Scanner scanner = new Scanner(connection.getInputStream());
                resposta = scanner.next();

                Log.e("FretadosSocorro", "informar percurso: " + resposta);
            }
            return resposta;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }




}
